#hc-nannon-offset-5-8-challenge.txt
for game in nannon minigammon
do
  for trial in `seq 0 9`
  do
    for p in 1.00 0.75 0.50 0.25 0.00
    do
      mv "hc-$game-p-$p-$trial-challenge.txt" "hc-challenge-$game-p-$p-$trial.txt"
    done
    for offset in `seq 0 6` 
    do
      mv "hc-$game-offset-$offset-$trial-challenge.txt" "hc-challenge-$game-offset-$offset-$trial.txt"
    done
  done
done
