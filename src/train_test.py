'''
Created on Dec 5, 2011

@author: reza
'''

import math
import random

from pybrain.datasets.supervised import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers.backprop import BackpropTrainer
from pybrain.structure.modules.sigmoidlayer import SigmoidLayer

X_MIN = -3.0
X_MAX = 3.0

TRAINING_SAMPLES = 200
PLOT_SAMPLES_PER_AXIS = 101

def fn(x):
    return math.sin(2 * x) + math.atan(x)

if __name__ == '__main__':
    # generate random points for training
    # save the points in a file for plotting
    f_input = open('../data/input.txt', 'w')
    train_dataset = SupervisedDataSet(1, 1)
    for i in range(TRAINING_SAMPLES):
        x = random.uniform(X_MIN, X_MAX)
        y = fn(x)
        train_dataset.addSample([x], [y])
        f_input.write("%f %f\n" % (x, y))
    f_input.close()
    
    # generate random points for evaluation
    eval_dataset = SupervisedDataSet(1, 1)
    for i in range(TRAINING_SAMPLES):
        x = random.uniform(X_MIN, X_MAX)
        y = fn(x)
        eval_dataset.addSample([x], [y])

    # create neural network and backprop trainer
    value_network = buildNetwork(1, 10, 1, hiddenclass = SigmoidLayer, bias = True)
    value_trainer = BackpropTrainer(value_network, learningrate = 0.01, 
                              momentum = 0.0, verbose = True)
    
    print value_network
    
    # train the network on training dataset
    print 'Value MSE before: %.4f' % value_trainer.testOnData(eval_dataset)
#    value_trainer.trainOnDataset(train_dataset, 100)
    value_trainer.trainUntilConvergence(train_dataset, maxEpochs = 200)
    print 'Value MSE after: %.4f' % value_trainer.testOnData(eval_dataset)

    # print final weights
    print "Final network weights:"
    print value_network.params

    # query the network on points on a grid
    f_value = open('../data/value.txt', 'w')
    plot_dataset = SupervisedDataSet(2, 1)
    plot_unit = (X_MAX - X_MIN) / (PLOT_SAMPLES_PER_AXIS - 1)
    for i in range(PLOT_SAMPLES_PER_AXIS):
        x = X_MIN + i * plot_unit
        z = value_network.activate([x])
        f_value.write("%f %f\n" % (x, z))
    f_value.close()

    