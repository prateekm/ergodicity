#!/bin/bash

#prog=minigammon.py
#prog=nannon.py

prog=$1

echo "Running $prog..."

python $prog p 1.00 0
python $prog p 0.75 0
python $prog p 0.50 0
python $prog p 0.25 0
python $prog p 0.00 0
